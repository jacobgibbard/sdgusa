					<div class="instagram-footer">
						<h5><span><a href="http://www.instagram.com/sdginnovations" target="_blank">#SDGINNOVATIONS</a></span></h5>			
						<?php echo do_shortcode('[instagram-feed id="245337752"]'); ?>
					</div>

					<footer class="footer" role="contentinfo">
						<div id="inner-footer" class="row">
							<div class="large-3 medium-12 columns">
								<ul>
									<a href="/"><img src="/dev/wp-content/uploads/footer-logo.png" class="footer-logo" /></a>
									<li class="contactinfo">Call Us: <a href="tel:+19497525227">(949) 752-5227</a></li>
									<li class="contactinfo">2806 Willis St‎<br />Santa Ana, CA 92705</li>
								</ul>
		    				</div>
		    				<div class="large-2 medium-12 columns">
								<div class="data-block-one">
									<h6 class="trigger">Information</h6>
			 						<ul class="data-collapse">
										<li><a href="/hours/">Hours &amp; Location</a></li>
										<li><a href="/about/">About</a></li>
										<li><a href="http://www.instagram.com/sdginnovations" target="_blank">#SDGINNOVATIONS</a></li>
										<li><a href="/contact/">Contact</a></li>
										<li><a href="/sitemap/">Sitemap</a></li>
									</ul>
								</div>
		    				</div>
		    				<div class="large-2 medium-12 columns">
								<div class="data-block-two">
									<h6 class="trigger">Store Links</h6>
									<ul class="data-collapse">
										<li><a href="/product-category/mx/">MX</a></li>
										<li><a href="/product-category/atv/">ATV</a></li>
										<li><a href="/product-category/accessories/">Accessories</a></li>
									</ul>
								</div>
		    				</div>
		    				<div class="large-2 medium-12 columns">
								<div class="data-block-three">
									<h6 class="trigger">My Account</h6>
									<ul class="data-collapse">									
										<li><a href="/shop/cart/">My Cart</a></li>
										<li><a href="/shop/my-account/">Account</a></li>
										<li><a href="/shop/checkout/">Checkout</a></li>
										<li><a href="/shop/shipping-returns/">Shipping/Returns</a></li>
									</ul>
		    					</div>
		    				</div>
		    				<div class="large-3 medium-12 columns">
								<ul>
									<li><h6>Stay Connected</h6></li>
									<li><a href="http://www.facebook.com/sdginnovations/" target="_blank"><i class="fa fa-facebook-square"></i></a> <a href="http://www.instagram.com/sdginnovations" target="_blank"><i class="fa fa-instagram"></i></a></li>
								</ul>
		    				</div>
							<div class="large-12 medium-12 columns">
								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> | All Rights Reserved | Site by <a href="http://www.gibbardwebdesign.com/" target="_blank">GWD</a></p>
							</div>
						</div> <!-- end #inner-footer -->
					</footer> <!-- end .footer -->
				</div> <!-- end #container -->
			</div> <!-- end .inner-wrap -->
		</div> <!-- end .off-canvas-wrap -->
		<?php wp_footer(); ?>
		<script type="text/javascript">
		    $(".data-block-one h6").click(function () {
		    $('.data-block-one .data-collapse').toggleClass('show');
		});		    
	    </script>
	    <script type="text/javascript">
		    $(".data-block-two h6").click(function () {
		    $('.data-block-two .data-collapse').toggleClass('show');
		});		    
	    </script>
	    <script type="text/javascript">
		    $(".data-block-three h6").click(function () {
		    $('.data-block-three .data-collapse').toggleClass('show');
	    });		
	    </script>
	</body>
</html> <!-- end page -->