<?php
// Theme support options
require_once(get_template_directory().'/assets/functions/theme-support.php');  

// WP Head and other cleanup functions
require_once(get_template_directory().'/assets/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/assets/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/assets/functions/menu.php'); 
require_once(get_template_directory().'/assets/functions/menu-walkers.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/assets/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/assets/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/assets/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/assets/translation/translation.php'); 

// Adds site styles to the WordPress editor
//require_once(get_template_directory().'/assets/functions/editor-styles.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/assets/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/assets/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/assets/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/assets/functions/admin.php'); 

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

// Display 20 products per page
add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 20;' ), 20 );


add_filter( 'woocommerce_package_rates', 'hide_shipping_based_on_tag' ,    10, 1 );

function check_cart_for_share() {

// specify the category id's you want to hide free_shipping
 $category_ID = array(
    '249', '271', '278', '277', '273', '258', '268', '254', '270', '252', '255', '716', '251', '257', '256',
 );
global $woocommerce;
$cart = $woocommerce->cart->cart_contents;

$found = false;

// loop through the array looking for the categories. Switch to true if the category is found.
  foreach ($woocommerce->cart->cart_contents as $key => $values ) {
        $terms = get_the_terms( $values['product_id'], 'product_cat' );
        foreach ($terms as $term) {
            if( in_array( $term->term_id, $category_ID ) ) {

        $found = true;
        break;
    }
  }
}

return $found;

}

function hide_shipping_based_on_tag( $available_methods ) {

// use the function above to check the cart for the categories.
if ( check_cart_for_share() ) {

    // remove the method you want
    unset( $available_methods['free_shipping'] ); // Replace "free_shipping" with the shipping option that you want to remove.
}

// return the available methods without the one you unset.
return $available_methods;

}




/**
 * woocommerce_package_rates is a 2.1+ hook
 */
add_filter( 'woocommerce_package_rates', 'hide_shipping_when_free_is_available', 10, 2 );
 
/**
 * Hide shipping rates when free shipping is available
 *
 * @param array $rates Array of rates found for the package
 * @param array $package The package array/object being shipped
 * @return array of modified rates
 */
function hide_shipping_when_free_is_available( $rates, $package ) {
    
    // Only modify rates if free_shipping is present
    if ( isset( $rates['free_shipping'] ) ) {
    
        // To unset a single rate/method, do the following. This example unsets flat_rate shipping
        unset( $rates['flat_rate'] );
        
        // To unset all methods except for free_shipping, do the following
        $free_shipping          = $rates['free_shipping'];
        $rates                  = array();
        $rates['free_shipping'] = $free_shipping;
    }
    
    return $rates;
}







function replacePayPalIcon($iconUrl) {
    return '/wp-content/uploads/paypal.gif';
}
add_filter('woocommerce_paypal_icon', 'replacePayPalIcon');


function flush_rules(){
flush_rewrite_rules();
}
add_action('init','flush_rules');

?>