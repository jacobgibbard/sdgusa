<?php get_header(); ?>

<?php putRevSlider("homepage","homepage") ?>
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
					 
					    <h2 class="subslidertext">Speed <span>Defies Gravity</span></h2>
						<div class="large-6 medium-6 small-12 columns right product-boxes">
							<?php echo do_shortcode('[product_categories columns="3" ids="6,26,12,18,20,28"]') ?>
						</div>	
						<div class="large-6 medium-6 small-12 columns left google-map">
							<?php get_template_part( 'parts/loop', 'home' ); ?>
						</div>												
						<div class="large-12 medium-12 columns hottest-products">
							<h3><span>Hottest Products</span></h3>
							<ul class="hottest-categories">
								<li><a href="/dev/product-category/mx/">MX</a></li>
								<li><a href="/dev/product-category/atv/">ATV</a></li>
								<li><a href="/dev/product-category/accessories/">Accessories</a></li>
							</ul>
							<div class="row">
								<?php echo do_shortcode('[recent_products per_page="3" columns="3"]') ?>
							</div>	
						</div>
						
				    </div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->			

<?php get_footer(); ?>