<?php get_header(); ?>
			
			<div id="content">
			
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-12 medium-12 columns" role="main">
					

								<?php while (woocommerce_content()) : the_post(); ?>
									<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
										<?php if ( function_exists('yoast_breadcrumb') ) {yoast_breadcrumb('<p id="breadcrumbs">','</p>');} ?>
										<header>
											<h1 class="entry-title"><?php the_title(); ?></h1>
										</header>
										<?php do_action('foundationPress_page_before_entry_content'); ?>
										<div class="entry-content">
											<?php the_content(); ?>
										</div>
										<footer>
											<?php wp_link_pages(array('before' => '<nav id="page-nav"><p>' . __('Pages:', 'FoundationPress'), 'after' => '</p></nav>' )); ?>
											<p><?php the_tags(); ?></p>
										</footer>
										<?php do_action('foundationPress_page_before_comments'); ?>
										<?php comments_template(); ?>
										<?php do_action('foundationPress_page_after_comments'); ?>
									</article>
								<?php endwhile;?>

					    					
    				</div> <!-- end #main -->
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>