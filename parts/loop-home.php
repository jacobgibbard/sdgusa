<?php query_posts('cat=29'); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(''); ?> role="article" itemscope itemtype="http://schema.org/BlogPosting">
					
    <section class="entry-content" itemprop="articleBody">
		<?php the_post_thumbnail('full'); ?>
		<h3 class="entry-title home-title" itemprop="headline"><?php the_title(); ?></h3>
		<?php the_content('Read More'); ?>
	</section> <!-- end article section -->
													
</article> <!-- end article -->

<?php wp_reset_query(); ?>