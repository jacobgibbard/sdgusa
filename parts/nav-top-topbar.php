<div class="contain-to-grid">
	<nav class="top-bar" data-topbar>
		<ul class="title-area">
			<!-- Title Area -->
			<li class="name">
				<a href="<?php echo home_url(); ?>" rel="nofollow"><img src="/dev/wp-content/uploads/logo.png" /></a>
			</li>
			<li class="toggle-topbar">
				<a href="#"><i class="fa fa-bars"></i></a>
			</li>
		</ul>
		<span class="headercontact"><a href="tel:+19497525227"><i class="fa fa-mobile"></i> (949) 752-5227</a> <a href="/contact/" target="_blank"><i class="fa fa-map-marker"></i> Directions</a> <a href="http://www.facebook.com/sdginnovations/" target="_blank"><i class="fa fa-facebook-square"></i></a> <a href="http://www.instagram.com/sdginnovations" target="_blank"><i class="fa fa-instagram"></i></a></h3></span>	
		<section class="top-bar-section">
			<?php joints_top_nav(); ?>
		</section>
	</nav>
</div>
